package th.co.ktb.next.archetype.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import th.co.ktb.next.common.base.BaseResponse;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GreetingsResponse extends BaseResponse {

    private String message;
}
