package th.co.ktb.next.archetype.interceptor;

import org.springframework.integration.config.GlobalChannelInterceptor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Component;
import th.co.ktb.next.archetype.util.GreetingsStreams;

@Component
@GlobalChannelInterceptor(patterns = GreetingsStreams.OUTPUT)
public class OutboundInterceptor implements ChannelInterceptor{

    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {

        System.out.println("POST SEND INTERCEPTOR : " + message.getPayload());
    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        System.out.println("PRE SEND INTERCEPTOR : " + message.getPayload());
        return message;
    }
}
