package th.co.ktb.next.archetype.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import th.co.ktb.next.common.base.BaseRequest;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GreetingsRequest extends BaseRequest {

    private long timestamp;
    private String message;
}
