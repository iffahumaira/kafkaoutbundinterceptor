package th.co.ktb.next.archetype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
* This class serves as main application.
* It provides Telemetry and Swagger capability with @EnableTelemetry and @EnableSwagger
* */

@SpringBootApplication
//@EnableTelemetry
//@EnableSwagger
public class KafkaOutboundApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaOutboundApplication.class, args);
	}

}
