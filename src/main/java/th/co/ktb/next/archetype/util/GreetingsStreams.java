package th.co.ktb.next.archetype.util;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface GreetingsStreams {

    String OUTPUT = "outbound";
    String INPUT = "inbound";

    @Output(OUTPUT)
    MessageChannel outboundGreetings();

    @Input(INPUT)
    MessageChannel inboundGreetings();
}
