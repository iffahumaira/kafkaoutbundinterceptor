package th.co.ktb.next.archetype.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;
import th.co.ktb.next.archetype.adaptor.Adaptor;
import th.co.ktb.next.archetype.interceptor.OutboundInterceptor;
import th.co.ktb.next.archetype.model.request.GreetingsRequest;
import th.co.ktb.next.archetype.model.response.GreetingsResponse;
import th.co.ktb.next.archetype.util.GreetingsStreams;
import th.co.ktb.next.common.exception.BusinessException;
import th.co.ktb.next.common.service.BaseService;

@Log4j2
@Service
public class OutboundInterceptorBaseService implements BaseService<GreetingsRequest, GreetingsResponse> {

    private Adaptor adaptor;
    private GreetingsStreams greetingsStreams;

    public OutboundInterceptorBaseService(Adaptor adaptor, GreetingsStreams greetingsStreams) {
        this.adaptor = adaptor;
        this.greetingsStreams = greetingsStreams;
    }

    @Override
    public GreetingsResponse execute(GreetingsRequest greetingsRequest) {

        GreetingsRequest input = GreetingsRequest.builder()
                .message(greetingsRequest.getMessage())
                .timestamp(System.currentTimeMillis())
                .build();

        OutboundInterceptor outboundInterceptor = new OutboundInterceptor();

        MessageChannel messageChannel = greetingsStreams.outboundGreetings();

        Message<GreetingsRequest> msg = MessageBuilder.withPayload(input)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build();

//        outboundInterceptor.postSend(msg, messageChannel, false);
//        outboundInterceptor.preSend(msg, messageChannel);

        return adaptor.messageInterceptor(input);
    }
}
