package th.co.ktb.next.archetype.filter;

import java.io.Serializable;

public class Packet implements Serializable {

    private String message;
    private long timestamp;
}
