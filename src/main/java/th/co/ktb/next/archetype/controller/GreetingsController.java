package th.co.ktb.next.archetype.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import th.co.ktb.next.archetype.model.request.GreetingsRequest;
import th.co.ktb.next.archetype.model.response.GreetingsResponse;
import th.co.ktb.next.archetype.service.GreetingsBaseService;
import th.co.ktb.next.archetype.service.OutboundInterceptorBaseService;

@RestController
public class GreetingsController {

    private GreetingsBaseService greetingsBaseService;
    private OutboundInterceptorBaseService outboundInterceptorBaseService;

    public GreetingsController(GreetingsBaseService greetingsBaseService, OutboundInterceptorBaseService outboundInterceptorBaseService) {
        this.greetingsBaseService = greetingsBaseService;
        this.outboundInterceptorBaseService = outboundInterceptorBaseService;
    }

    @GetMapping("/greetings")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public GreetingsResponse getGreetingsMessage (@RequestBody GreetingsRequest message){
        return greetingsBaseService.execute(message);
    }

    @GetMapping("/greetingsInterceptor")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public GreetingsResponse greetingsInterceptor (@RequestBody GreetingsRequest message){
        return outboundInterceptorBaseService.execute(message);
    }
}
