package th.co.ktb.next.archetype.listener;

import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import th.co.ktb.next.archetype.model.request.GreetingsRequest;
import th.co.ktb.next.archetype.model.response.GreetingsResponse;
import th.co.ktb.next.archetype.service.ListenerBaseService;
import th.co.ktb.next.archetype.util.GreetingsStreams;

@Component
@Log4j2
@EnableBinding(GreetingsStreams.class)
public class GreetingsListener {

    private ListenerBaseService listenerBaseService;

    public GreetingsListener(ListenerBaseService listenerBaseService) {
        this.listenerBaseService = listenerBaseService;
    }

//    @StreamListener(GreetingsStreams.INPUT)
//    @SendTo(GreetingsStreams.OUTPUT)
//    public GreetingsResponse sendToMessage(GreetingsRequest greetings){
//        log.info("RECEIVED FROM LISTENER : {}", greetings);
//        return listenerBaseService.execute(greetings);
//    }

//    @StreamListener(GreetingsStreams.INPUT)
//    public void sendToMessage(GreetingsRequest greetings){
//        log.info("RECEIVED FROM LISTENER : {}", greetings);
//        listenerBaseService.execute(greetings);
//    }
}
