package th.co.ktb.next.archetype.model.entity;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class Greetings {

    private long timestamp;
    private String message;
}
