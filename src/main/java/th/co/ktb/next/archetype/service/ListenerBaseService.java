package th.co.ktb.next.archetype.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import th.co.ktb.next.archetype.model.request.GreetingsRequest;
import th.co.ktb.next.archetype.model.response.GreetingsResponse;
import th.co.ktb.next.common.service.BaseService;

@Log4j2
@Service
public class ListenerBaseService implements BaseService<GreetingsRequest, GreetingsResponse> {

    @Override
    public GreetingsResponse execute(GreetingsRequest greetingsRequest) {
        log.info("RECEIVED FROM BASE SERVICE : {} AND {}", greetingsRequest.getMessage(), greetingsRequest.getTimestamp());

        GreetingsResponse response = new GreetingsResponse();
        response.setMessage(greetingsRequest.getMessage());

        return response;
    }
}
