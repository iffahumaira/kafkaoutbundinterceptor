package th.co.ktb.next.archetype.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import th.co.ktb.next.archetype.adaptor.Adaptor;
import th.co.ktb.next.archetype.model.request.GreetingsRequest;
import th.co.ktb.next.archetype.model.response.GreetingsResponse;
import th.co.ktb.next.archetype.util.GreetingsStreams;
import th.co.ktb.next.common.service.BaseService;

@Log4j2
@Service
public class GreetingsBaseService implements BaseService<GreetingsRequest, GreetingsResponse> {

    private Adaptor adaptor;
    private GreetingsStreams greetingsStreams;

    public GreetingsBaseService(Adaptor adaptor, GreetingsStreams greetingsStreams) {
        this.adaptor = adaptor;
        this.greetingsStreams = greetingsStreams;
    }

    @Override
    public GreetingsResponse execute(GreetingsRequest greetingsRequest) {

        GreetingsRequest input = GreetingsRequest.builder()
                .message(greetingsRequest.getMessage())
                .timestamp(System.currentTimeMillis())
                .build();

        return adaptor.sendMessage(input);
    }
}
