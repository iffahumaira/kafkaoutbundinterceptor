package th.co.ktb.next.archetype.adaptor.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.util.MimeTypeUtils;
import th.co.ktb.next.archetype.adaptor.Adaptor;
import th.co.ktb.next.archetype.interceptor.OutboundInterceptor;
import th.co.ktb.next.archetype.model.request.GreetingsRequest;
import th.co.ktb.next.archetype.model.response.GreetingsResponse;
import th.co.ktb.next.archetype.util.GreetingsStreams;

@Log4j2
@EnableBinding(GreetingsStreams.class)
public class AdaptorImpl implements Adaptor {

    private final GreetingsStreams greetingsStreams;

    public AdaptorImpl(GreetingsStreams greetingsStreams) {
        this.greetingsStreams = greetingsStreams;
    }

    @Override
    public GreetingsResponse sendMessage(GreetingsRequest greetingsRequest) {

        MessageChannel messageChannel = greetingsStreams.outboundGreetings();

        messageChannel.send(MessageBuilder
                .withPayload(greetingsRequest)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());

        GreetingsResponse response = new GreetingsResponse();
        response.setMessage("Greetings message : " + greetingsRequest.getMessage() +
                            " Greetings timestamp : " + greetingsRequest.getTimestamp());

        return response;
    }

    @Override
    public GreetingsResponse messageInterceptor(GreetingsRequest greetingsRequest) {

        GreetingsResponse response = new GreetingsResponse();
        response.setMessage("Greetings message : " + greetingsRequest.getMessage() +
                " Greetings timestamp : " + greetingsRequest.getTimestamp());

        MessageChannel messageChannel = greetingsStreams.outboundGreetings();

        Message<GreetingsRequest> msg = MessageBuilder.withPayload(greetingsRequest)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build();

        OutboundInterceptor outboundInterceptor = new OutboundInterceptor();

//        outboundInterceptor.postSend(msg, messageChannel, true);
        outboundInterceptor.preSend(msg, messageChannel);

        return response;
    }
}
