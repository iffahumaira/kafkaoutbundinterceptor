package th.co.ktb.next.archetype.adaptor;

import th.co.ktb.next.archetype.model.request.GreetingsRequest;
import th.co.ktb.next.archetype.model.response.GreetingsResponse;

public interface Adaptor {

    GreetingsResponse sendMessage (GreetingsRequest greetingsRequest);
    GreetingsResponse messageInterceptor (GreetingsRequest greetingsRequest);
}
